from datetime import datetime
from airflow import DAG
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.python_operator import PythonOperator
# new line in hello world
def list_number():
    items=[2,6,7,8,12,5,4,3,2]
    for item in items:
        print(item)
dag = DAG('hello_world', description='Hello World DAG',
          schedule_interval='*/15 * * * *',
          start_date=datetime(2022, 3, 11), catchup=False)

hello_operator = PythonOperator(task_id='hello_task', python_callable=list_number, dag=dag)

hello_operator
