from datetime import datetime
from airflow import DAG
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.python_operator import PythonOperator
import time

def Pwc():
        time.sleep(60)
        print('This prints once a minute123.')

dag = DAG('test_dag', description='Hello World DAG 2',
        schedule_interval='*/3 * * * *',
        start_date=datetime(2022, 3, 28), catchup=False)



hello_operator = PythonOperator(task_id='hello_task', python_callable=Pwc, dag=dag)

hello_operator