#! /bin/bash

Commit1=$(git whatchanged --diff-filter=A | awk ' {print $2 }'|head -1)
Commit2=$(git whatchanged --diff-filter=A| awk ' {print $2 }' |head -9|tail -1)
Commit3=$(git whatchanged --diff-filter=A| awk ' {print $2 }' |head -17|tail -1)
Filename1=$(git show --pretty="" --name-only $Commit1|tail -1)
Filename2=$(git show --pretty="" --name-only $Commit2|tail -1)
Filename3=$(git show --pretty="" --name-only $Commit3|tail -1)

Commit4=$(git whatchanged --diff-filter=D | awk ' {print $2 }'|head -1)
Commit5=$(git whatchanged --diff-filter=D | awk ' {print $2 }'|head -9|tail -1)
Commit6=$(git whatchanged --diff-filter=D | awk ' {print $2 }'|head -17|tail -1)
Filename4=$(git show --pretty="" --name-only $Commit4|tail -1)
Filename5=$(git show --pretty="" --name-only $Commit5|tail -1)
Filename6=$(git show --pretty="" --name-only $Commit6|tail -1)

Commit7=$(git whatchanged --diff-filter=M | awk ' {print $2 }'|head -1)
Commit8=$(git whatchanged --diff-filter=M | awk ' {print $2 }'|head -9|tail -1)
Commit9=$(git whatchanged --diff-filter=M | awk ' {print $2 }'|head -17|tail -1)
Filename7=$(git show --pretty="" --name-only $Commit7|tail -1)
Filename8=$(git show --pretty="" --name-only $Commit8|tail -1)
Filename9=$(git show --pretty="" --name-only $Commit9|tail -1)

echo $Filename1 '| A' >>t11.txt
echo $Filename2 '| A' >>t11.txt
echo $Filename3 '| A' >>t11.txt
echo $Filename4 '| D' >>t11.txt
echo $Filename5 '| D' >>t11.txt
echo $Filename6 '| D' >>t11.txt
echo $Filename7 '| M' >>t11.txt
echo $Filename8 '| M' >>t11.txt
echo $Filename9 '| M' >>t11.txt