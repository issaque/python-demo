#!/bin/bash

#Based on the latest commitId in Bitbucket we are taking files names which got changed in repo
CommitID=$(git rev-parse HEAD)
dt=`date +%Y%m%d%H%M%S`
ENVIRONMENT=composer-test-env
REGION=us-west1
#Filenames=/jenkins-slave/test/workspace/Python-Demo/test_$dt.txt
Filenames=/var/lib/jenkins/workspace/Python-Demo/test_$dt.txt
git log --name-status --diff-filter="ACDMRT" -1 $CommitID| grep -ve '^$' | grep -ve 'Author' | grep -ve '^Date' |grep -ve '^commit'|sed '1d' >>$Filenames

#In the below code we are finding the all new added dags in bitbucket repo and copying into GCP storage
cd /var/lib/jenkins/workspace/Python-Demo
filename=test_$dt.txt
if [ -f "$filename" ]; then
        echo "$filename exists."
        for file in `awk '$1 ~ /^A/ {print $2}' $filename`;
        do
                 if [ "${file: -4}" == ".sql" ]; then
                        file1=`basename $file`
                        gsutil cp -r $file gs://us-west1-composer-test-env-2baa409a-bucket/dags/DRL_SQL/
                        echo "Adding a SQL file into the gcp cloud bucket"
                else
                        gsutil cp -r $file gs://us-west1-composer-test-env-2baa409a-bucket/dags/
                        echo "Dags got Added in the gcp Cloud buckect"
                fi
        done
#        exit 0
else
        echo "There is no dags for adding in the cloud buckect"
        exit 1
fi