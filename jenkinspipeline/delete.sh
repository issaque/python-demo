#!/bin/bash

#Based on the latest commitId in Bitbucket we are taking dags name and SQL files which got changed in repo
CommitID=$(git rev-parse HEAD)
dt=`date +%Y%m%d%H%M%S`
ENVIRONMENT=composer-test-env
REGION=us-west1
Filenames=/var/lib/jenkins/workspace/Python-Demo/test_$dt.txt
git log --name-status --diff-filter="ACDMRT" -1 $CommitID| grep -ve '^$' | grep -ve 'Author' | grep -ve '^Date' |grep -ve '^commit'|sed '1d' >>$Filenames

cd /var/lib/jenkins/workspace/Python-Demo
filename=test_$dt.txt
if [ -f "$filename" ]; then
        echo "$filename exists."
        for file in `awk '$1 ~ /^D/ {print $2}' $filename`;
        do
                if [ "${file: -4}" == ".sql" ]; then
                        file1=`basename $file`
                        gsutil rm gs://us-west1-composer-test-env-2baa409a-bucket/dags/DRL_SQL/$file1
                        echo "Deleting SQL file from the GCP cloud bucket"
                else
                        DagFile=`basename $file .py`
                        status=`gcloud -q composer environments run "$ENVIRONMENT" --location "$REGION" dags list-runs -- -d "$DagFile" |awk -F '|' 'NR==4{print $3}'`
                        while [[ $status == "running" ]]
                        do
                                echo "Dag is running"
                                sleep 5s
                        done
                                gsutil rm gs://us-west1-composer-test-env-2baa409a-bucket/dags/$file
                                echo "Dags is removed from the gcp cloud bucket"
                fi
        done
        exit 0
else
        echo "$filename is not exists"
        exit 1
fi